import { StringSchema, ObjectSchema } from 'yup';
interface Prop {
    name: string;
    validator: StringSchema<string | undefined>;
    placeholder?: string;
    label?: string;
    type?: string;
}
interface ParamsSetField {
    validator?: StringSchema<string>;
    placeholder?: string;
    label?: string;
    type?: string;
}
export declare class FormValidation {
    private static schemaMap;
    static registerValidation(target: any, propertyKey: string, validator?: StringSchema<string>): void;
    static registerFieldData(target: any, propertyKey: string, data?: ParamsSetField): void;
    static getKeys(target: any): Prop[];
    static getSchema(metatype: any): ObjectSchema<object | undefined> | undefined;
    static getFormSchema(metatype: any): {
        name: string;
        placeholder: string;
        label: string;
        type: string;
    }[];
    static transform(value: any, metatype: any): {};
}
export {};
