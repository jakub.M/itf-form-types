"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Email = void 0;
const validation_1 = require("../validation");
const yup_1 = require("yup");
exports.Email = () => (target, propertyKey) => validation_1.FormValidation.registerFieldData(target, propertyKey, {
    validator: yup_1.string().required().email(),
    placeholder: 'example@gmail.com',
    label: 'Email',
    type: 'email',
});
//# sourceMappingURL=email.decorator.js.map