"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Pwd2 = void 0;
const validation_1 = require("../validation");
const yup_1 = require("yup");
exports.Pwd2 = (pwdRef) => (target, propertyKey) => validation_1.FormValidation.registerFieldData(target, propertyKey, {
    validator: yup_1.string().oneOf([yup_1.ref(pwdRef !== null && pwdRef !== void 0 ? pwdRef : 'pwd'), null], 'Passwords do not match').required('Confirm password is required'),
    placeholder: '********',
    label: 'Repeat password',
    type: 'password',
});
//# sourceMappingURL=pwd2.decorator.js.map