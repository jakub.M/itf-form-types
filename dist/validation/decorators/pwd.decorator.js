"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Pwd = void 0;
const validation_1 = require("../validation");
const yup_1 = require("yup");
exports.Pwd = (length) => (target, propertyKey) => validation_1.FormValidation.registerFieldData(target, propertyKey, {
    validator: yup_1.string().required().min(length !== null && length !== void 0 ? length : 8),
    placeholder: '********',
    label: 'Password',
    type: 'password',
});
//# sourceMappingURL=pwd.decorator.js.map