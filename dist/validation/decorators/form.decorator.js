"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Form = void 0;
const validation_1 = require("../validation");
exports.Form = (data) => (target, propertyKey) => validation_1.FormValidation.registerValidation(target, propertyKey, data);
//# sourceMappingURL=form.decorator.js.map