"use strict";
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FormValidation = void 0;
const yup_1 = require("yup");
class FormValidation {
    static registerValidation(target, propertyKey, validator) {
        this
            .getKeys(target)
            .push({ name: propertyKey, validator: validator !== null && validator !== void 0 ? validator : yup_1.string() });
    }
    static registerFieldData(target, propertyKey, data) {
        const { validator } = data, restParams = __rest(data, ["validator"]);
        this
            .getKeys(target)
            .push(Object.assign({ name: propertyKey, validator: validator !== null && validator !== void 0 ? validator : yup_1.string() }, restParams));
    }
    static getKeys(target) {
        const keyName = target.constructor.name;
        let keys = this.schemaMap.get(keyName);
        if (!keys) {
            keys = [];
            this.schemaMap.set(keyName, keys);
        }
        return keys;
    }
    static getSchema(metatype) {
        const schemaProps = this.schemaMap.get(metatype.name);
        if (!schemaProps) {
            return undefined;
        }
        return yup_1.object().shape(schemaProps.reduce((res, prop) => (Object.assign(Object.assign({}, res), { [prop.name]: prop.validator })), {}));
    }
    static getFormSchema(metatype) {
        const schemaProps = this.schemaMap.get(metatype.name);
        return schemaProps === null || schemaProps === void 0 ? void 0 : schemaProps.map(prop => {
            const { name, type, placeholder, label } = prop;
            return { name, placeholder, label, type: type !== null && type !== void 0 ? type : 'string' };
        });
    }
    static transform(value, metatype) {
        return this
            .schemaMap
            .get(metatype.name)
            .reduce((res, { name }) => (Object.assign(Object.assign({}, res), { [name]: value[name] })), {});
    }
}
exports.FormValidation = FormValidation;
FormValidation.schemaMap = new Map();
//# sourceMappingURL=validation.js.map