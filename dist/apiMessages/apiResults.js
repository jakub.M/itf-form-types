"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApiResult = void 0;
var ApiResult;
(function (ApiResult) {
    ApiResult[ApiResult["error"] = 0] = "error";
    ApiResult[ApiResult["ok"] = 1] = "ok";
})(ApiResult = exports.ApiResult || (exports.ApiResult = {}));
//# sourceMappingURL=apiResults.js.map