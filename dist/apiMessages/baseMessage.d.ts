import { ApiErrorMessage, ApiOkMessage } from "./messageTypes";
export declare type BaseResultMessage<T> = ApiErrorMessage | ApiOkMessage<T>;
