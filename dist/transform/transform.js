"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Transform = void 0;
class Transform {
    static registerProp(target, propertyKey) {
        const keyName = target.constructor.name;
        let keys = this.propsMap.get(keyName);
        if (!keys) {
            keys = [];
            this.propsMap.set(keyName, keys);
        }
        keys.push(propertyKey);
    }
    static getProps(metatype) {
        return this.propsMap.get(metatype.name);
    }
}
exports.Transform = Transform;
Transform.propsMap = new Map();
//# sourceMappingURL=transform.js.map