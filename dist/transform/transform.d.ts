export declare class Transform {
    private static propsMap;
    static registerProp(target: any, propertyKey: string): void;
    static getProps(metatype: any): string[];
}
