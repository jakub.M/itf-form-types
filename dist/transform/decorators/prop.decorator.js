"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Prop = void 0;
const transform_1 = require("../transform");
exports.Prop = (data) => (target, propertyKey) => transform_1.Transform.registerProp(target, propertyKey);
//# sourceMappingURL=prop.decorator.js.map