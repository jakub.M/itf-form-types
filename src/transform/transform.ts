export class Transform {
  private static propsMap: Map<any, string[]> = new Map();

  static registerProp(target: any, propertyKey: string): void {
    const keyName = target.constructor.name;
    let keys = this.propsMap.get(keyName);
    if (!keys) {
      keys = [];
      this.propsMap.set(keyName, keys);
    }
    keys.push( propertyKey );
  }
  static getProps(metatype: any) {
    return this.propsMap.get(metatype.name);
  }
}
