import { Transform } from '../transform';

export const Prop = (data?: any) => (target: any, propertyKey: string) => Transform.registerProp(target, propertyKey);