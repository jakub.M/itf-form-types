import { FormValidation } from '../validation';
import { string } from 'yup';

export const Email = () =>
  (target: any, propertyKey: string) =>
    FormValidation.registerFieldData(
      target,
      propertyKey,
      { 
        validator: string().required().email(),
        placeholder: 'example@gmail.com',
        label: 'Email',
        type: 'email',
      }
    );
