import { FormValidation } from '../validation';
import { StringSchema } from 'yup';

export const Form = (data?: StringSchema<string>) =>
  (target: any, propertyKey: string) =>
    FormValidation.registerValidation(target, propertyKey, data );
