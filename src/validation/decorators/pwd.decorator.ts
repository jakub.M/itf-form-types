import { FormValidation } from '../validation';
import { string } from 'yup';

export const Pwd = (length?: number) =>
  (target: any, propertyKey: string) =>
    FormValidation.registerFieldData(
      target,
      propertyKey,
      { 
        validator: string().required().min(length ?? 8),
        placeholder: '********',
        label: 'Password',
        type: 'password',
      }
    );
