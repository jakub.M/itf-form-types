import { FormValidation } from '../validation';
import { string, ref } from 'yup';

export const Pwd2 = (pwdRef?: string) =>
  (target: any, propertyKey: string) =>
    FormValidation.registerFieldData(
      target,
      propertyKey,
      { 
        validator: string().oneOf([ref(pwdRef ?? 'pwd'), null], 'Passwords do not match').required('Confirm password is required'),
        placeholder: '********',
        label: 'Repeat password',
        type: 'password',
      }
    );
