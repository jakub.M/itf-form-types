import { string, StringSchema, object, ObjectSchema } from 'yup';

interface Prop {
  name: string;
  validator: StringSchema<string | undefined>;
  placeholder?: string;
  label?: string;
  type?: string;
}

interface ParamsSetField {
  validator?: StringSchema<string>;
  placeholder?: string;
  label?: string;
  type?: string;
}

export class FormValidation {
  private static schemaMap: Map<any, Prop[]> = new Map();

  static registerValidation(target: any, propertyKey: string, validator?: StringSchema<string>): void {
    this
    .getKeys(target)
    .push({ name: propertyKey, validator: validator ?? string() });
  }

  static registerFieldData(target: any, propertyKey: string, data?: ParamsSetField): void {
    const { validator, ...restParams } = data;
    this
    .getKeys(target)
    .push({ name: propertyKey, validator: validator ?? string(), ...restParams });
  }

  static getKeys (target: any) {
    const keyName = target.constructor.name
    let keys = this.schemaMap.get(keyName);
    if (!keys) {
      keys = [];
      this.schemaMap.set(keyName, keys);
    }
    return keys;
  }

  static getSchema(metatype: any): ObjectSchema<object | undefined> | undefined {
    const schemaProps = this.schemaMap.get(metatype.name);
    if (!schemaProps) {
      return undefined;
    }
    return object().shape(schemaProps.reduce((res, prop) => ({ ...res, [prop.name]: prop.validator }), {}))
  }

  static getFormSchema(metatype: any) {
    const schemaProps = this.schemaMap.get(metatype.name);
    return schemaProps?.map(prop => {
      const { name, type, placeholder, label} = prop;
      return { name, placeholder, label, type: type ?? 'string' }
    })
  }

  static transform(value: any, metatype: any) {
    return this
      .schemaMap
      .get(metatype.name)
      .reduce((res, { name }) => ({ ...res, [name]: value[name] }) ,{})
  }
}
