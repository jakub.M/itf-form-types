import { Email, Pwd, Pwd2 } from '../validation/decorators';

export class AuthLoginReq {
  constructor(){}
  @Email()
  email: string;
  @Pwd()
  pwd: string;
  @Pwd2()
  pwd2: string;
}
